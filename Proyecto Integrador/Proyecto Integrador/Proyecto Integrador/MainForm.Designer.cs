﻿/*
 * Creado por SharpDevelop.
 * Usuario: Usuario
 * Fecha: 17/10/2021
 * Hora: 00:04
 * 
 * Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
 */
namespace Proyecto_Integrador
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MonthCalendar MthCalendario;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox CmbBx1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbBxHorario;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.ComboBox cmbprof;
		private System.Windows.Forms.Label label6;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.MthCalendario = new System.Windows.Forms.MonthCalendar();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.CmbBx1 = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbBxHorario = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmbprof = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// MthCalendario
			// 
			this.MthCalendario.Location = new System.Drawing.Point(18, 18);
			this.MthCalendario.Name = "MthCalendario";
			this.MthCalendario.TabIndex = 0;
			this.MthCalendario.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.ThCaleDateChanged);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Red;
			this.button1.Location = new System.Drawing.Point(503, 301);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(65, 39);
			this.button1.TabIndex = 1;
			this.button1.Text = "Cancelar";
			this.button1.UseVisualStyleBackColor = false;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Green;
			this.button2.Location = new System.Drawing.Point(592, 301);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(67, 39);
			this.button2.TabIndex = 2;
			this.button2.Text = "Añadir";
			this.button2.UseVisualStyleBackColor = false;
			// 
			// CmbBx1
			// 
			this.CmbBx1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CmbBx1.FormattingEnabled = true;
			this.CmbBx1.Location = new System.Drawing.Point(74, 103);
			this.CmbBx1.Name = "CmbBx1";
			this.CmbBx1.Size = new System.Drawing.Size(120, 21);
			this.CmbBx1.TabIndex = 8;
			this.CmbBx1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(7, 105);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 23);
			this.label1.TabIndex = 9;
			this.label1.Text = "Obra social:";
			// 
			// cmbBxHorario
			// 
			this.cmbBxHorario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbBxHorario.FormattingEnabled = true;
			this.cmbBxHorario.Location = new System.Drawing.Point(74, 141);
			this.cmbBxHorario.Name = "cmbBxHorario";
			this.cmbBxHorario.Size = new System.Drawing.Size(120, 21);
			this.cmbBxHorario.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(7, 144);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 21);
			this.label2.TabIndex = 11;
			this.label2.Text = "Horario:";
			this.label2.Click += new System.EventHandler(this.Label2Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(7, 171);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 23);
			this.label3.TabIndex = 13;
			this.label3.Text = "Fecha:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(7, 30);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(61, 23);
			this.label4.TabIndex = 14;
			this.label4.Text = "Nombre:";
			this.label4.Click += new System.EventHandler(this.Label4Click);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(7, 67);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(61, 23);
			this.label5.TabIndex = 15;
			this.label5.Text = "Apellido:";
			this.label5.Click += new System.EventHandler(this.Label5Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cmbprof);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.dateTimePicker1);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.textBox2);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.cmbBxHorario);
			this.groupBox1.Controls.Add(this.CmbBx1);
			this.groupBox1.Location = new System.Drawing.Point(300, 18);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(359, 244);
			this.groupBox1.TabIndex = 16;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Paciente";
			// 
			// cmbprof
			// 
			this.cmbprof.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbprof.FormattingEnabled = true;
			this.cmbprof.Location = new System.Drawing.Point(74, 205);
			this.cmbprof.Name = "cmbprof";
			this.cmbprof.Size = new System.Drawing.Size(120, 21);
			this.cmbprof.TabIndex = 19;
			this.cmbprof.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(7, 205);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(66, 23);
			this.label6.TabIndex = 18;
			this.label6.Text = "Profesional:";
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePicker1.Location = new System.Drawing.Point(74, 171);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(120, 20);
			this.dateTimePicker1.TabIndex = 17;
			this.dateTimePicker1.ValueChanged += new System.EventHandler(this.DateTimePicker1ValueChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButton4);
			this.groupBox2.Controls.Add(this.radioButton3);
			this.groupBox2.Controls.Add(this.radioButton2);
			this.groupBox2.Controls.Add(this.radioButton1);
			this.groupBox2.Location = new System.Drawing.Point(226, 24);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(109, 141);
			this.groupBox2.TabIndex = 17;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Motivo";
			// 
			// radioButton4
			// 
			this.radioButton4.Location = new System.Drawing.Point(7, 111);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(104, 24);
			this.radioButton4.TabIndex = 21;
			this.radioButton4.TabStop = true;
			this.radioButton4.Text = "Otro";
			this.radioButton4.UseVisualStyleBackColor = true;
			// 
			// radioButton3
			// 
			this.radioButton3.Location = new System.Drawing.Point(7, 81);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(104, 24);
			this.radioButton3.TabIndex = 20;
			this.radioButton3.TabStop = true;
			this.radioButton3.Text = "Receta medica";
			this.radioButton3.UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(6, 20);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(104, 24);
			this.radioButton2.TabIndex = 19;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Consulta";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// radioButton1
			// 
			this.radioButton1.Location = new System.Drawing.Point(6, 50);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(105, 24);
			this.radioButton1.TabIndex = 18;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Control rutinario";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(74, 67);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(120, 20);
			this.textBox2.TabIndex = 17;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(74, 30);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(120, 20);
			this.textBox1.TabIndex = 16;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(687, 352);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.MthCalendario);
			this.Name = "MainForm";
			this.Text = "Clinica";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
	}
}
