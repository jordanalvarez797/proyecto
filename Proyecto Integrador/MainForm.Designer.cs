﻿/*
 * Creado por SharpDevelop.
 * Usuario: Usuario
 * Fecha: 17/10/2021
 * Hora: 00:04
 * 
 * Para cambiar esta plantilla use Herramientas | Opciones | Codificación | Editar Encabezados Estándar
 */
namespace Proyecto_Integrador
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MonthCalendar MthCalendario;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox CmbBx1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbBxHorario;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.ComboBox cmbprof;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox grpBxMetodosdepago;
		private System.Windows.Forms.TextBox txtBxMontoapagar;
		private System.Windows.Forms.RadioButton rdBttnEfectivo;
		private System.Windows.Forms.RadioButton rdBttnTarjeta;
		private System.Windows.Forms.TextBox txtBxNdeobra;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtBxNdetarjeta;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox Recordatorio;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox textBox3;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.MthCalendario = new System.Windows.Forms.MonthCalendar();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.CmbBx1 = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbBxHorario = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton6 = new System.Windows.Forms.RadioButton();
			this.radioButton5 = new System.Windows.Forms.RadioButton();
			this.label8 = new System.Windows.Forms.Label();
			this.txtBxNdeobra = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.cmbprof = new System.Windows.Forms.ComboBox();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.label6 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.grpBxMetodosdepago = new System.Windows.Forms.GroupBox();
			this.txtBxNdetarjeta = new System.Windows.Forms.TextBox();
			this.txtBxMontoapagar = new System.Windows.Forms.TextBox();
			this.rdBttnEfectivo = new System.Windows.Forms.RadioButton();
			this.rdBttnTarjeta = new System.Windows.Forms.RadioButton();
			this.Recordatorio = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.grpBxMetodosdepago.SuspendLayout();
			this.Recordatorio.SuspendLayout();
			this.SuspendLayout();
			// 
			// MthCalendario
			// 
			this.MthCalendario.Location = new System.Drawing.Point(16, 16);
			this.MthCalendario.Name = "MthCalendario";
			this.MthCalendario.TabIndex = 0;
			this.MthCalendario.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.ThCaleDateChanged);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Red;
			this.button1.Location = new System.Drawing.Point(552, 272);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(65, 39);
			this.button1.TabIndex = 1;
			this.button1.Text = "Cancelar";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Green;
			this.button2.Location = new System.Drawing.Point(656, 272);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(67, 39);
			this.button2.TabIndex = 2;
			this.button2.Text = "Aceptar";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Enter += new System.EventHandler(this.MainFormLoad);
			// 
			// CmbBx1
			// 
			this.CmbBx1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CmbBx1.FormattingEnabled = true;
			this.CmbBx1.Location = new System.Drawing.Point(80, 88);
			this.CmbBx1.Name = "CmbBx1";
			this.CmbBx1.Size = new System.Drawing.Size(120, 21);
			this.CmbBx1.TabIndex = 8;
			this.CmbBx1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 88);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 23);
			this.label1.TabIndex = 9;
			this.label1.Text = "Obra social:";
			// 
			// cmbBxHorario
			// 
			this.cmbBxHorario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbBxHorario.FormattingEnabled = true;
			this.cmbBxHorario.Location = new System.Drawing.Point(256, 152);
			this.cmbBxHorario.Name = "cmbBxHorario";
			this.cmbBxHorario.Size = new System.Drawing.Size(64, 21);
			this.cmbBxHorario.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(208, 152);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 21);
			this.label2.TabIndex = 11;
			this.label2.Text = "Horario:";
			this.label2.Click += new System.EventHandler(this.Label2Click);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 152);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 23);
			this.label3.TabIndex = 13;
			this.label3.Text = "Fecha:";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(8, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(73, 23);
			this.label4.TabIndex = 14;
			this.label4.Text = "Nombre:";
			this.label4.Click += new System.EventHandler(this.Label4Click);
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 56);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(73, 23);
			this.label5.TabIndex = 15;
			this.label5.Text = "Apellido:";
			this.label5.Click += new System.EventHandler(this.Label5Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButton6);
			this.groupBox1.Controls.Add(this.radioButton5);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.txtBxNdeobra);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.cmbprof);
			this.groupBox1.Controls.Add(this.dateTimePicker1);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.textBox2);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.cmbBxHorario);
			this.groupBox1.Controls.Add(this.CmbBx1);
			this.groupBox1.Location = new System.Drawing.Point(280, 24);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(456, 230);
			this.groupBox1.TabIndex = 16;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Paciente";
			// 
			// radioButton6
			// 
			this.radioButton6.Location = new System.Drawing.Point(256, 48);
			this.radioButton6.Name = "radioButton6";
			this.radioButton6.Size = new System.Drawing.Size(40, 24);
			this.radioButton6.TabIndex = 24;
			this.radioButton6.TabStop = true;
			this.radioButton6.Text = "No";
			this.radioButton6.UseVisualStyleBackColor = true;
			// 
			// radioButton5
			// 
			this.radioButton5.Location = new System.Drawing.Point(216, 48);
			this.radioButton5.Name = "radioButton5";
			this.radioButton5.Size = new System.Drawing.Size(40, 24);
			this.radioButton5.TabIndex = 23;
			this.radioButton5.TabStop = true;
			this.radioButton5.Text = "Si";
			this.radioButton5.UseVisualStyleBackColor = true;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(216, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 24);
			this.label8.TabIndex = 22;
			this.label8.Text = "Historial medico";
			// 
			// txtBxNdeobra
			// 
			this.txtBxNdeobra.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.txtBxNdeobra.Location = new System.Drawing.Point(80, 120);
			this.txtBxNdeobra.Name = "txtBxNdeobra";
			this.txtBxNdeobra.Size = new System.Drawing.Size(120, 20);
			this.txtBxNdeobra.TabIndex = 21;
			this.txtBxNdeobra.Tag = "";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 120);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 23);
			this.label7.TabIndex = 20;
			this.label7.Text = "N° de obra:";
			// 
			// cmbprof
			// 
			this.cmbprof.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbprof.FormattingEnabled = true;
			this.cmbprof.Location = new System.Drawing.Point(80, 184);
			this.cmbprof.Name = "cmbprof";
			this.cmbprof.Size = new System.Drawing.Size(120, 21);
			this.cmbprof.TabIndex = 19;
			this.cmbprof.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePicker1.Location = new System.Drawing.Point(80, 152);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(120, 20);
			this.dateTimePicker1.TabIndex = 17;
			this.dateTimePicker1.ValueChanged += new System.EventHandler(this.DateTimePicker1ValueChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButton4);
			this.groupBox2.Controls.Add(this.radioButton3);
			this.groupBox2.Controls.Add(this.radioButton2);
			this.groupBox2.Controls.Add(this.radioButton1);
			this.groupBox2.Location = new System.Drawing.Point(336, 24);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(109, 141);
			this.groupBox2.TabIndex = 17;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Motivo";
			this.groupBox2.Enter += new System.EventHandler(this.GroupBox2Enter);
			// 
			// radioButton4
			// 
			this.radioButton4.Location = new System.Drawing.Point(7, 111);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(89, 24);
			this.radioButton4.TabIndex = 21;
			this.radioButton4.TabStop = true;
			this.radioButton4.Text = "Otro";
			this.radioButton4.UseVisualStyleBackColor = true;
			// 
			// radioButton3
			// 
			this.radioButton3.Location = new System.Drawing.Point(7, 81);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(89, 24);
			this.radioButton3.TabIndex = 20;
			this.radioButton3.TabStop = true;
			this.radioButton3.Text = "Receta medica";
			this.radioButton3.UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(6, 20);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(90, 24);
			this.radioButton2.TabIndex = 19;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Consulta";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// radioButton1
			// 
			this.radioButton1.Location = new System.Drawing.Point(6, 50);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(98, 24);
			this.radioButton1.TabIndex = 18;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Control rutinario";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(8, 184);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(72, 23);
			this.label6.TabIndex = 18;
			this.label6.Text = "Profesional:";
			this.label6.Click += new System.EventHandler(this.Label6Click);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(80, 56);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(120, 20);
			this.textBox2.TabIndex = 17;
			this.textBox2.TextChanged += new System.EventHandler(this.TextBox2TextChanged);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(80, 24);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(120, 20);
			this.textBox1.TabIndex = 16;
			// 
			// grpBxMetodosdepago
			// 
			this.grpBxMetodosdepago.Controls.Add(this.txtBxNdetarjeta);
			this.grpBxMetodosdepago.Controls.Add(this.txtBxMontoapagar);
			this.grpBxMetodosdepago.Controls.Add(this.rdBttnEfectivo);
			this.grpBxMetodosdepago.Controls.Add(this.rdBttnTarjeta);
			this.grpBxMetodosdepago.Location = new System.Drawing.Point(12, 190);
			this.grpBxMetodosdepago.Name = "grpBxMetodosdepago";
			this.grpBxMetodosdepago.Size = new System.Drawing.Size(200, 80);
			this.grpBxMetodosdepago.TabIndex = 17;
			this.grpBxMetodosdepago.TabStop = false;
			this.grpBxMetodosdepago.Text = "Metodo de pago";
			this.grpBxMetodosdepago.Enter += new System.EventHandler(this.GrpBxMetodosdepagoEnter);
			// 
			// txtBxNdetarjeta
			// 
			this.txtBxNdetarjeta.Location = new System.Drawing.Point(88, 16);
			this.txtBxNdetarjeta.Name = "txtBxNdetarjeta";
			this.txtBxNdetarjeta.Size = new System.Drawing.Size(100, 20);
			this.txtBxNdetarjeta.TabIndex = 3;
			this.txtBxNdetarjeta.Text = "N° de tarjeta";
			// 
			// txtBxMontoapagar
			// 
			this.txtBxMontoapagar.Location = new System.Drawing.Point(88, 48);
			this.txtBxMontoapagar.Name = "txtBxMontoapagar";
			this.txtBxMontoapagar.Size = new System.Drawing.Size(100, 20);
			this.txtBxMontoapagar.TabIndex = 2;
			this.txtBxMontoapagar.Text = "Ingrese monto";
			this.txtBxMontoapagar.TextChanged += new System.EventHandler(this.TxtBxMontoapagarTextChanged);
			// 
			// rdBttnEfectivo
			// 
			this.rdBttnEfectivo.Location = new System.Drawing.Point(8, 48);
			this.rdBttnEfectivo.Name = "rdBttnEfectivo";
			this.rdBttnEfectivo.Size = new System.Drawing.Size(64, 24);
			this.rdBttnEfectivo.TabIndex = 1;
			this.rdBttnEfectivo.TabStop = true;
			this.rdBttnEfectivo.Text = "Efectivo";
			this.rdBttnEfectivo.UseVisualStyleBackColor = true;
			// 
			// rdBttnTarjeta
			// 
			this.rdBttnTarjeta.Location = new System.Drawing.Point(8, 16);
			this.rdBttnTarjeta.Name = "rdBttnTarjeta";
			this.rdBttnTarjeta.Size = new System.Drawing.Size(64, 24);
			this.rdBttnTarjeta.TabIndex = 0;
			this.rdBttnTarjeta.TabStop = true;
			this.rdBttnTarjeta.Text = "Tarjeta";
			this.rdBttnTarjeta.UseVisualStyleBackColor = true;
			// 
			// Recordatorio
			// 
			this.Recordatorio.Controls.Add(this.button3);
			this.Recordatorio.Controls.Add(this.textBox3);
			this.Recordatorio.Location = new System.Drawing.Point(288, 260);
			this.Recordatorio.Name = "Recordatorio";
			this.Recordatorio.Size = new System.Drawing.Size(232, 56);
			this.Recordatorio.TabIndex = 18;
			this.Recordatorio.TabStop = false;
			this.Recordatorio.Text = "Recordatorio";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(144, 24);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(72, 24);
			this.button3.TabIndex = 1;
			this.button3.Text = "Establecer";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(8, 24);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(100, 20);
			this.textBox3.TabIndex = 0;
			this.textBox3.Text = "Ingrese Email";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.MenuHighlight;
			this.ClientSize = new System.Drawing.Size(759, 324);
			this.Controls.Add(this.Recordatorio);
			this.Controls.Add(this.grpBxMetodosdepago);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.MthCalendario);
			this.Name = "MainForm";
			this.Text = "Clinica Privada ";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.grpBxMetodosdepago.ResumeLayout(false);
			this.grpBxMetodosdepago.PerformLayout();
			this.Recordatorio.ResumeLayout(false);
			this.Recordatorio.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
